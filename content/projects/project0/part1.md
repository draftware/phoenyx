+++
title = "Title of project 0 part 1"
description = "Description of project 0 part 1"

weight = 1

[taxonomies]
categories = ["Project"]
tags       = ["Part", "1"]
+++

Project 0 part 1 summary.

<!-- more -->

Project 0 part 1 content.
