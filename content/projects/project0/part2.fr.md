+++
title = "Titre de la partie 2 du projet 0"
description = "Description de la partie 2 du projet 0"

weight = 2

[taxonomies]
categories = ["Projet"]
tags       = ["Partie", "2"]
+++

*Teaser* de la seconde partie du projet 0.

<!-- more -->

Contenu de la seconde partie du projet 0.
