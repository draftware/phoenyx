+++
title = "Title of project 1 part 1"
description = "Description of project 1 part 1"

weight = 1

[taxonomies]
categories = ["Project"]
tags       = ["Part", "1"]
+++

Project 1 part 1 summary.

<!-- more -->

Project 1 part 1 content.
