+++
title = "Title of project 1 part 2"
description = "Description of project 1 part 2"

weight = 2

[taxonomies]
categories = ["Project"]
tags       = ["Part", "2"]
+++

Project 1 part 2 summary.

<!-- more -->

Project 1 part 2 content.
