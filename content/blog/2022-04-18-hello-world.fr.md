+++
title = "Bonjour tout le monde"
description = "Description de la publication de bienvenue"

[taxonomies]
categories = ["Zola"]
tags       = ["Bonjour", "monde"]
+++

Hello world :wave:

Il s'agit de la publication la plus simple possible

<!-- more -->

La partie avant `<!-- more -->` est présentée comme un [sommaire](https://www.getzola.org/documentation/content/page/#summary) sur les autres pages.
