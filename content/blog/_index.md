+++
title = "Blog"
description = "Blog description"

weight = 0

sort_by = "date"
paginate_by = 3

[extra]
[[extra.banner]]
theme = "dark"
url = "https://images.ctfassets.net/hrltx12pl8hq/5596z2BCR9KmT1KeRBrOQa/4070fd4e2f1a13f71c2c46afeb18e41c/shutterstock_451077043-hero1.jpg"

[[extra.banner]]
theme = "light"
url = "https://www.industrialempathy.com/img/remote/ZiClJf-1920w.jpg"
+++
