+++
title = "Hello world"
description = "Hello world post's description"

[taxonomies]
categories = ["Zola"]
tags       = ["Hello", "world"]
+++

Hello world :wave:

This is the simplest post example you can find here.

<!-- more -->

The part before the `<!-- more -->` creates a [summary](https://www.getzola.org/documentation/content/page/#summary) used as a preview in other pages.
