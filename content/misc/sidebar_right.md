+++
title = "Right sidebar"
description = "How to customize the right sidebar"

[taxonomies]
categories = ["Phoenyx"]
tags = ["right", "sidebar", "customize"]
+++

This page describes hot to customize the right sidebar.

By default, the right side contains the *ToC*[^toc] if available.
This means it will be empty for *section* and *taxonomy* pages but visible for *page* pages.

Here is how to change [Phœnyx][phoenyx]'s behavior regarding this side bar.

<!-- more -->

## Remove

The easiest way to get rid of the right sidebar content is to add the following snippet in you `template/base.html`:

```html,linenos
{%- block right -%}
{%- endblock right -%}
```
The `aside` tag will still be there in order to preserve [Phœnyx][phoenyx]'s design.
But it'll be empty of content.

## Replace

There are two ways to replace the right sidebar content.
You can either add a custom `template/right.html` of your own making which will then be imported instead of the [default](./#) one.
Or you can directly write some code inside the `block` as follow:

```html,linenos
{%- block right -%}
<!-- Custom code -->
{%- endblock right -%}
```

[^toc]: Table of Content

[phoenyx]: / "Phœnyx"
