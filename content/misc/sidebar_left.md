+++
title = "Left sidebar"
description = "How to customize the left sidebar"

[taxonomies]
categories = ["Phoenyx"]
tags = ["left", "sidebar", "customize"]

[extra]

[[extra.thumbnail]]
theme = "all"
src = "https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/4692e9108512257.5fbf40ee3888a.jpg"

[[extra.banner]]
theme = "dark"
url = "https://www.publicdomainpictures.net/pictures/320000/nahled/background-image.png"

[[extra.banner]]
theme = "light"
url = "https://static.addtoany.com/images/dracaena-cinnabari.jpg"
+++

This page describes how to customize the left sidebar.

By default, the left sidebar contains a list of taxonomy *kinds* and *terms*.
Not only you can chose what *kind* appears here by tweaking the `config.toml` file.
But the whole content is also easy to set depending on the context using [zola][zola]'s powerful template system.

Here is how.

<!-- more -->

## Default behavior

As stated before, the default behavior of the left sidebar is to show the available taxonomy *kinds* and *terms*.
And those *kinds* are globally defined in the main section of the `config.toml` file.

But wait, there is more!
You can also specify a **subset** of those *kinds* in the language specifying section of this very file.

Doing so tells [Phœnyx][phoenyx] which *kinds* should be visible in the left sidebar.

### Example

```toml,linenos
taxonomies = [
    { name = "categories", lang = "en"},
    { name = "tags"      , lang = "en"},
    { name = "categories", lang = "fr"},
    { name = "tags"      , lang = "fr"},
]

[languages.en]
taxonomies = [
    { name = "categories"},
    { name = "tags"      },
]

[languages.fr]
taxonomies = [
    { name = "categories"},
]
```

Here, there are two *kinds* available both in English and French.
So, any page can have either or both of the `categories` and `tags` *kinds*.

But, while the left sidebar of the English version will show both *kinds*, the French one will only show the `categories`.

## Overwrite

The default behavior is defined in the `template/left.html` file which is included in `template/base.html` as follow:

```html,linenos
<aside id="left">
    {%- block left -%}
    {%- include "left.html" ignore missing -%}
    {%- endblock left -%}
</aside>
```

### Remove

If you want to fully remove the content of the left sidebar, you can simply add the following snippet in your own templates:

```html,linenos
{%- block left -%}
{%- endblock left -%}
```

This will keep the `aside` tag in order to preserve [Phœnyx][phoenyx]'s design but it'll be empty of content.

### Replace

There are two ways to replace the left sidebar content.
You can either add a custom `template/left.html` of your own which will then be imported instead of the [default](./#default-behavior) one.
Or you can directly write some code inside the `block` as follow:

```html,linenos
{%- block left -%}
<!-- Custom code -->
{%- endblock left -%}
```

[zola]: https://getzola.org "Zola's website"
[phoenyx]: / "Phœnyx"
