let light = "light";
let dark = "dark";

function set_theme(theme) {
    document.documentElement.setAttribute("data-theme", theme);
    localStorage.setItem("data-theme", theme);
}

function toogle_theme() {
    set_theme(document.documentElement.getAttribute("data-theme") === dark ? light : dark);
}

let user_prefers_dark = window.matchMedia('(prefers-color-scheme: dark)').matches;
let local_data_theme = localStorage.getItem("data-theme");

if (local_data_theme) {
    // User explicit preference.
    set_theme(local_data_theme);
} else if (user_prefers_dark) {
    // User default preference.
    set_theme(dark);
}

document.getElementById("toggle_theme").addEventListener("click", toogle_theme);
